Напишите реализацию класса `Functor` для ассоциативного списка:

```haskell
data AssocList k v = AssocList [(k, v)] deriving (Show)
```

