{-# LANGUAGE DeriveDataTypeable #-}
module Homework (
  AssocList(..),
) where

import Data.Data

data AssocList k v = AssocList [(k, v)] deriving (Eq, Show, Data, Typeable)

