{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
import Test.Hspec
import Test.QuickCheck
import Test.ChasingBottoms
import Control.Exception (evaluate)

import Homework

main :: IO ()
main = hspec $ do
  describe "instance Functor (AssocList k)" $ do
    it "fmap id = id" $ do
      property $ \(xs :: [(Int, Int)]) -> fmap id (AssocList xs) == AssocList xs
    it "fmap works for infinite lists" $ do
      property $ \n (k :: Int) (v :: Int) -> take' n (fmap (+ 1) (AssocList $ repeat (k, v))) ~~ AssocList (replicate n (k, v))
  where
    (~~) = semanticEq noTweak { timeOutLimit = Just 1 }
    take' n (AssocList xs) = AssocList (take n xs)

